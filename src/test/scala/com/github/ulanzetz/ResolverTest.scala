package com.github.ulanzetz

import org.scalatest.wordspec.AnyWordSpec

class ResolverTest extends AnyWordSpec {
  "Resolver" should {
    "work with 1" in {
      assert(Resolver.resolve(1) == "yes")
    }
  }
}
