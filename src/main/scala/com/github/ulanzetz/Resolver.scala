package com.github.ulanzetz

object Resolver {
  def resolve(int: Int): String =
    int match {
      case 1 => "yes"
      case 2 => "no"
      case 4 => "kek"
      case _ => "undefined"
    }
}
